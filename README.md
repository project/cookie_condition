CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides a condition plugin for cookies, e.g. you can setup cookie name & value
so your block will be shown only for users with set cookies or vice versa, you
can also use it everywhere, where conditions plugins are supported.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/cookie_condition

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/cookie_condition


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Cookie Condition module as you would normally install
   a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, it provides condition plugin which can be selected from appropriate
interface select list.


MAINTAINERS
-----------

 * Pavel Ruban - https://www.drupal.org/u/pavel-ruban

Supporting organizations:

 * FFW Agency - https://www.drupal.org/ffw-agency
