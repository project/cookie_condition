<?php

namespace Drupal\cookie_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Pass checks if cookie is present and its value equals to a defined one.
 *
 * @Condition(
 *   id = "cookie",
 *   label = @Translation("Cookie"),
 *   description = @Translation("Allows to select a particular cookie with a specific value.")
 * )
 */
class Cookie extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Creates a new Cookie instance.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(RequestStack $request_stack, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('request_stack'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['cookie_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie Name'),
      '#default_value' => $this->configuration['cookie_name'],
    ];

    $form['operator'] = [
      '#title' => $this->t('Operator'),
      '#type' => 'select',
      '#options' => [
        'is' => '=',
        'contains' => 'contains'
      ],
      '#default_value' => $this->configuration['operator'],
    ];

    $form['cookie_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie Value'),
      '#default_value' => $this->configuration['cookie_value'],
    ];

    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['cookie_name'] = $form_state->getValue('cookie_name');
    $this->configuration['operator'] = $form_state->getValue('operator');
    $this->configuration['cookie_value'] = $form_state->getValue('cookie_value');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cookie_name' => '',
      'operator' => 'is',
      'cookie_value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    if ($this->configuration['cookie_name']) {
      $contexts[] = 'cookies:' . $this->configuration['cookie_name'];
    }
    return $contexts;
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {
    $cookie_name = $this->configuration['cookie_name'];
    $operator = $this->configuration['operator'];
    $cookie_value = $this->configuration['cookie_value'];

    if (!$cookie_name && !$this->isNegated()) {
      return TRUE;
    }

    $cookies = $this->requestStack->getCurrentRequest()->cookies;

    if ($operator === 'is' ) {
      return $cookies->get($cookie_name) === $cookie_value;
    }

    return strpos($cookies->get($cookie_name), $cookie_value) !== false;

  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    $placeholders = [
      '@name' => $this->configuration['cookie_name'],
      '@operator' => $this->configuration['operator'],
      '@value' => $this->configuration['cookie_value'],
    ];

    return $this->isNegated()
      ? $this->t('Cookie "@name" is NOT equal to "@value"', $placeholders)
      : $this->t('Cookie "@name" has "@value" value', $placeholders);
  }

}
